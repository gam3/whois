#--
# Ruby Whois
#
# An intelligent pure Ruby WHOIS client and parser.
#
# Copyright (c) 2009-2016 Simone Carletti <weppos@weppos.net>
#++


require 'whois/version'
#require 'whois/errors'
#require 'whois/client'
#require 'whois/server'
require 'whois/record'
require 'json'
require 'pp'
require 'whois/server/adapter'

class Whois
  def initialize(path: Dir[File.expand_path(File.join(File.dirname(__FILE__), "../data/*.json"))])
    @data_path = path
    @tld_filename = File.expand_path(File.join(File.dirname(__FILE__), "../data/tld.json"))
    @tld_list = JSON.parse(File.open(@tld_filename, 'r').read, symbolize_names: true);
    @builder = Server::Adapter.new(adapters: @tld_adapters)
  end

  def lookup(domain)
    lookup_domain(domain)
  end

  def lookup_domain(domain)
    temp_name = domain.dup
    tld = nil
    while (temp_name.size > 0)
       if tld = @tld_list[temp_name.to_sym]
	 break
       end
       temp_name = temp_name.split('.')[1..-1].join('.')
    end
    record = nil
    if tld
      whois_host = tld[:host]
      options = tld[:options] || {}
      options[:parent] = Record.new('',  referer: whois_host)
      record = @builder.get(domain, whois_host, tld[:options] || {})
      while referer = record.referer
        record = @builder.get(domain, referer, parent: record)
      end
    else
      raise "not found #{domain}"
    end
    record
  end
end
