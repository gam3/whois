#--
# Ruby Whois
#
# An intelligent pure Ruby WHOIS client and parser.
#
# Copyright (c) 2009-2016 Simone Carletti <weppos@weppos.net>
# Copyright (c) 2016 G. Allen Morris III <gam3@gam3.net>
#++

require 'socket'

class Whois
  class Record
    # @return [Whois::Server] The server that originated this record.
    attr_reader :server

    # @return [Array<Whois::Record::Part>] The parts that compose this record.
    attr_reader :body

    attr_reader :parent

    attr_reader :domain
    attr_reader :server

    # Initializes a new instance with given +server+ and +parts+.
    #
    # @param  [Whois::Server] server
    # @param  [Array<Whois::Record::Part>] parts
    #
    def initialize(body, server: server, referer: referer, parent: parent, domain: domain)
      @body  = body
      @server = server
      @parent = parent
      @domain = domain
      raise ArgumentError, 'domain is required' unless domain
      if referer
	begin
	  Socket.gethostbyname(referer + '.')
	  @referer = referer
	rescue => x
	  warn "Ignoring referer because of network error: '#{referer}'"
	end
      end
    end

    def referer
      @referer
    end

    def inspect
      %Q|#\<Whois::Record:#{object_id} @server=\\"#{@server}\\" @referer=\\"#{@referer}\\" >|
    end
  end
end
