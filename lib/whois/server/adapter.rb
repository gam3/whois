#--
# Ruby Whois
#
# An intelligent pure Ruby WHOIS client and parser.
#
# Copyright (c) 2009-2016 Simone Carletti <weppos@weppos.net>
#++


#require 'whois/record/part'
require 'whois/record'
require 'whois/server/socket_handler'
require 'whois/tlds'
require 'net/telnet'

class Whois
  class Server
    class Adapter
      def initialize(options)
        @adapters = Whois::DEFAULT_ADAPTERS
      end
      def referer
        raise 'bob'
      end
      def get(domain, whois_server = nil, **options)
p [ domain, whois_server, options ]
	adapter = @adapters[whois_server.to_sym] || 'standard'
	require "whois/server/adapters/#{adapter}"
        class_name = adapter.to_s.split("_").collect(&:capitalize).join
	aclass = self.class.const_get(class_name)

	aclass.request(domain, whois_server, options);
      end

      class << self
	def query(query, host, port: 43)
	  localhost = Net::Telnet.new("Host" => host, "Timeout" => 10, "Port" => port)
	  data = localhost.cmd(query)
	  encoding = 'ISO-8859-15'
	  begin
	    data.force_encoding(encoding)
	  rescue => error
	    STDERR.puts "#{error}"
	  end
	  data
	end

	alias :query_the_socket :query
      end
    end
  end
end
