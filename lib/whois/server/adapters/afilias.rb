#--
# Ruby Whois
#
# An intelligent pure Ruby WHOIS client and parser.
#
# Copyright (c) 2009-2016 Simone Carletti <weppos@weppos.net>
#++


class Whois
  class Server
    class Adapter
      #
      # = Afilias Adapter
      #
      # Provides ability to query Afilias WHOIS interfaces.
      #

      class Afilias < Adapter
	def self.request(domain, server, **options)
	  response = query_the_socket(domain, server, options)
	  referer = response.slice(/Whois Server:(\S+)/, 1) if response =~ /Domain Name:/
          Record.new(response, server: server, referer: referer)
	end
      end
    end
  end
end
