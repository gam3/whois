#--
# Ruby Whois
#
# An intelligent pure Ruby WHOIS client and parser.
#
# Copyright (c) 2009-2016 Simone Carletti <weppos@weppos.net>
#++


module Whois
  class Server
    class Adapter

      #
      # = Arin Adapter
      #
      # Provides ability to query Arin WHOIS interfaces.
      #
      class Arin < Adapter

        # Executes a WHOIS query to the Arin WHOIS interface,
        # resolving any intermediate referral,
        # and appends the response to the client buffer.
        #
        # @param  [String] string
        # @return [void]
        #
        def request(string)
          response = query_the_socket("n + #{string}", host)
          buffer_append response, host

          if options[:referral] != false && (referral = extract_referral(response))
            response = query_the_socket(string, referral[:host], referral[:port])
            buffer_append(response, referral[:host])
          end
        end
      end
    end
  end
end
