#--
# Ruby Whois
#
# An intelligent pure Ruby WHOIS client and parser.
#
# Copyright (c) 2009-2016 Simone Carletti <weppos@weppos.net>
#++


class Whois
  class Server
    class Adapter
      #
      # = Verisign Adapter
      #
      # Provides ability to query Verisign WHOIS interfaces.
      #
      class Verisign < Adapter
        # Executes a WHOIS query to the Verisign WHOIS interface,
        # resolving any intermediate referral,
        # and appends the response to the client buffer.
        #
        # @param  [String] string
        # @return [void]
        #
        def self.request(domain, whois_host, **options)
          response = query_the_socket("=" + domain, whois_host)

          server = nil

	  referer = nil
	  if response =~ /Domain Name:/
            referer = response.scan(/Whois Server: (.+?)$/).flatten.last
            referer.strip! if referer != nil
            referer = nil  if referer == "not defined"
            referer
          end
          Record.new(response, server: whois_host, referer: referer, domain: domain)
        end
      end
    end
  end
end
