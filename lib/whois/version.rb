#--
# Ruby Whois
#
# An intelligent pure Ruby WHOIS client and parser.
#
# Copyright (c) 2009-2016 Simone Carletti <weppos@weppos.net>
#++


class Whois
  # The current library version.
  VERSION = "0.0.1-beta2"
end
